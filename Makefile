GO    := GO15VENDOREXPERIMENT=1 go

REPO_PATH               ?= gitlab.com/kornwilliams/common
TESTARGS                ?= -v -race
COVERARGS               ?= -coverprofile=profile.out -covermode=atomic
TEST                    ?= $(shell go list ./... | grep -v '/vendor/')
GOFMT_FILES             ?= $(shell find . -name '*.go' | grep -v vendor | xargs)
FIRST_GOPATH            := $(firstword $(subst :, ,$(shell $(GO) env GOPATH)))
DEP                     := $(FIRST_GOPATH)/bin/dep
GOCOV                   := $(FIRST_GOPATH)/bin/gocov
GOIMPORTS               := $(FIRST_GOPATH)/bin/goimports


export REPO_PATH

.PHONY: all
all: format test


$(DEP):
	@echo ">> installing golang dep tool"
	@$(GO) get -u "github.com/golang/dep/cmd/dep"


$(GOCOV):
	@echo ">> installing gocov tool"
	@$(GO) get -u "github.com/axw/gocov/gocov"


.PHONY: setup
setup:
	@echo ">> installing dependencies"
	@$(GO) get -u "github.com/alecthomas/gometalinter"
	@gometalinter --install --update


.PHONY: dep
dep: $(DEP)
	dep ensure


$(GOIMPORTS):
	@echo ">> installing goimports tool"
	@$(GO) get -u "github.com/golang/tools/cmd/goimports"


.PHONY: test
test:
	@echo ">> running tests"
	@$(GO) test $(TEST) $(TESTARGS)


.PHONY: cover
cover: $(GOCOV)
	@echo ">> running test coverage"
	@rm -f coverage.txt
	@EXIT_CODE=0; \
	for d in $(TEST); do \
		$(GO) test $(TESTARGS) $(COVERARGS) $$d; \
		code=$$?; [ "$$code" -ne 0 ] && EXIT_CODE=$$code; \
		if [ -f profile.out ]; then \
			if [ -f coverage.txt ]; then \
				tail -n +2 profile.out >> coverage.txt; \
			else \
				cat profile.out >> coverage.txt; \
			fi; \
			rm profile.out; \
		fi \
	done; \
	if [ -f coverage.txt ]; then \
		$(GO) tool cover -html coverage.txt -o coverage.html; \
		$(GOCOV) convert coverage.txt | gocov report; \
	fi; \
	exit $$EXIT_CODE


.PHONY: lint
lint:
	@echo ">> linting code"
	@gometalinter --vendor --disable-all \
		--enable=varcheck \
		--enable=gosimple \
		--enable=misspell \
		--enable=vet \
		--enable=vetshadow \
		--enable=golint \
		--deadline=10m \
		./...


.PHONY: fmtcheck
fmtcheck:
	@echo ">> checking code style"
	@gometalinter --vendor --disable-all \
		--enable=gofmt \
		--enable=goimports \
		./...


.PHONY: format
format: $(GOIMPORTS)
	@echo ">> formatting code"
	@$(GOIMPORTS) -local "$(REPO_PATH)" -w $(GOFMT_FILES)
