# log package

### log.level

Only log messages with the given severity or above. Valid levels:

- debug
- info
- warn
- error
- fatal

### log.format

The `log.format` have a common format(optional parts marked by squared brackets):

`logger:<encoder>[?param=value[&param2=value2]]`

A full example:

`logger:json?outputPaths=/var/log/test.log&disableCaller=false&disableStacktrace=false`

#### Encoders

Currently, only two encoders are supported:

1. JSON (Default)
2. Console

#### Parameters

##### development

##### disableCaller

##### disableStacktrace

##### outputPaths

##### lumberjack
