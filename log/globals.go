package log

import (
	"sync"

	"go.uber.org/zap"
)

var (
	_globalMu     sync.RWMutex
	_globalL      *zap.Logger
	_globalQuickL *zap.Logger
	_globalS      *zap.SugaredLogger
)

func init() {
	err := initGlobalLogger(defaultLogFormatURI)
	if err != nil {
		panic(err)
	}
}

func initGlobalLogger(format string) error {
	c, err := ParseConfigFromURIString(format)
	if err != nil {
		return err
	}

	l, err := c.Build()
	if err != nil {
		return err
	}
	ReplaceGlobals(l)
	return nil
}

// L returns the global zap.Logger.
//
// It's safe for concurrent use.
func L() *zap.Logger {
	_globalMu.RLock()
	l := _globalL
	_globalMu.RUnlock()
	return l
}

// S returns the global zap.SugaredLogger.
// ReplaceGlobals.
//
// It's safe for concurrent use.
func S() *zap.SugaredLogger {
	_globalMu.RLock()
	s := _globalS
	_globalMu.RUnlock()
	return s
}

// ReplaceGlobals replaces the global zap.Logger and the zap.SugaredLogger, and returns
// a function to restore the original values.
//
// It's safe for concurrent use.
func ReplaceGlobals(logger *zap.Logger) func() {
	_globalMu.Lock()
	prev := _globalL
	_globalL = logger
	_globalQuickL = logger.WithOptions(zap.AddCallerSkip(1))
	_globalS = logger.Sugar()
	_globalMu.Unlock()
	return func() { ReplaceGlobals(prev) }
}
