package flags

import "fmt"

// StringsFlag satisify flag.Value interface.
type StringsFlag struct {
	Default string
	values  []string
}

// String implements flag.Value interface.
func (s *StringsFlag) String() string {
	return fmt.Sprintf("%v", s.Values())
}

// Set implements flag.Value interface.
func (s *StringsFlag) Set(value string) error {
	s.values = append(s.values, value)
	return nil
}

// Values returns string slice.
func (s *StringsFlag) Values() []string {
	l := len(s.values)
	if l == 0 {
		return []string{s.Default}
	}
	r := make([]string, l)
	copy(r, s.values)
	return r
}
