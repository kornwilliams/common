package flags

import (
	"testing"
)

func TestParse(t *testing.T) {
	tests := []struct {
		input []string
		valid bool
	}{
		{
			input: []string{"--param.a", "testing"},
			valid: true,
		},
		{
			input: []string{"--xxxxx", "x"},
			valid: false,
		},
	}

	var paramA string
	flags := NewFlags("test")
	flags.FS.StringVar(&paramA, "param.a", "", "")

	for i, test := range tests {
		err := flags.Parse(test.input)
		if test.valid && err != nil {
			t.Errorf("%d. expected input to be valid, got %s", i, err)
		} else if !test.valid && err == nil {
			t.Errorf("%d. expected input to be invalid", i)
		}
	}
}
