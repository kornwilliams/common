package flags

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringsFlag(t *testing.T) {
	f := &StringsFlag{Default: "default"}
	assert.Equal(t, "[default]", f.String())
	assert.Equal(t, []string{"default"}, f.Values())

	vals := []string{
		"abc",
		"hij",
		"nif1234",
	}
	for _, val := range vals {
		assert.NoError(t, f.Set(val))
	}

	assert.Equal(t, fmt.Sprintf("%v", vals), f.String())
	assert.Equal(t, vals, f.Values())
}
