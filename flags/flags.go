package flags

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"text/template"
	"unicode"
)

// Flags is a wrapper for FlagSet, which is useful for both parsing and printing help information.
type Flags struct {
	FS *flag.FlagSet
}

// NewFlags returns new initialized Flags.
func NewFlags(program string) *Flags {
	fs := flag.NewFlagSet(program, flag.ContinueOnError)
	fs.Usage = FlagSetUsage(fs, program)
	return &Flags{FS: fs}
}

// Parse parses the flags.
func (f *Flags) Parse(args []string) error {
	err := f.FS.Parse(args)
	if err != nil || len(f.FS.Args()) != 0 {
		if err != flag.ErrHelp {
			fmt.Fprintf(os.Stderr, "Invalid command line arguments. Help: %s -h", os.Args[0])
		}
		if err == nil {
			err = fmt.Errorf("Non-flag argument on command line: %q", f.FS.Args()[0])
		}
		return err
	}

	return nil
}

var (
	helpTmpl       = template.Must(template.New("usage").Funcs(defaultTemplateFuncs).Parse(helpTmplString))
	helpTmplString = strings.TrimSpace(`
usage: {{ .Program }} [<args>]
{{ range $cat, $flags := .Groups }}{{ if ne $cat "." }} == {{ $cat | upper }} =={{ end }}
  {{ range $flags }}
   -{{ .Name }} {{ .DefValue | quote }}
      {{ .Usage | wrap 80 6 }}
  {{ end }}
{{ end }}
`)
	defaultTemplateFuncs = template.FuncMap{
		"wrap": func(width, indent int, s string) (ns string) {
			width = width - indent
			length := indent
			for _, w := range strings.SplitAfter(s, " ") {
				if length+len(w) > width {
					ns += "\n" + strings.Repeat(" ", indent)
					length = 0
				}
				ns += w
				length += len(w)
			}
			return strings.TrimSpace(ns)
		},
		"quote": func(s string) string {
			if len(s) == 0 || s == "false" || s == "true" || unicode.IsDigit(rune(s[0])) {
				return s
			}
			return fmt.Sprintf("%q", s)
		},
		"upper": strings.ToUpper,
	}
)

type templateData struct {
	Program string
	Groups  map[string][]*flag.Flag
}

// FlagSetUsage returns the usage func the specific FlagSet.
func FlagSetUsage(fs *flag.FlagSet, program string) func() {
	return func() {
		groups := make(map[string][]*flag.Flag)
		templateData := &templateData{
			Program: program,
			Groups:  groups,
		}

		// Bucket flags into groups based on the first of their dot-separated levels.
		fs.VisitAll(func(fl *flag.Flag) {
			parts := strings.SplitN(fl.Name, ".", 2)
			if len(parts) == 1 {
				groups["."] = append(groups["."], fl)
			} else {
				name := parts[0]
				groups[name] = append(groups[name], fl)
			}
		})
		for cat, fl := range groups {
			if len(fl) < 2 && cat != "." {
				groups["."] = append(groups["."], fl...)
				delete(groups, cat)
			}
		}

		if err := helpTmpl.Execute(os.Stdout, templateData); err != nil {
			panic(fmt.Errorf("error executing usage template: %s", err))
		}
	}
}
