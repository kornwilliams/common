# Common

This repository contains Go libraries that are shared.

- [flags](./flags): A wrapper for command flags parsing.
- [log](./log/README.md): A logging wrapper around [zap](https://github.com/uber-go/zap)
- [version](./version): Version information and metric
- [debugutil](.debugutil): Utils for debugging purpose
